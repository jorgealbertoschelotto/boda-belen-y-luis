import gspread
import pandas as pd
import firebase_admin
from firebase_admin import credentials, firestore



# Firebase to dataframe
if 'BodaBelyLuis' not in firebase_admin._apps:
        cred = credentials.Certificate("./key.json")
        firebase_admin.initialize_app(cred, 
        {
        'databaseURL': 'https://boda-bel-luis.firebaseio.com/'
        })
db = firestore.client()

users = list(db.collection(u'invitados').stream())

users_dict = list(map(lambda x: x.to_dict(), users))
df = pd.DataFrame(users_dict).fillna('')




# GOOGLE SHEET
gc = gspread.service_account(filename='googlesheets.json')

# Open a sheet from a spreadsheet in one go
wks = gc.open("invitados bel luis").sheet1
print(df.columns.values.tolist())
print(df.values.tolist())

# Update a range of cells using the top left corner address
wks.update([df.columns.values.tolist()] + df.values.tolist())