const firebaseConfig = {
    apiKey: "AIzaSyBrluFUxyvgsRG9wbyHy9oPQASDT0_XcrI",
    authDomain: "boda-bel-luis.firebaseapp.com",
    projectId: "boda-bel-luis",
    storageBucket: "boda-bel-luis.appspot.com",
    messagingSenderId: "156843290075",
    appId: "1:156843290075:web:37f3dab71940b952258bfe",
};
$("body").fadeIn(2000);

class Productos {
    constructor(db, actualRef) {
        this.productosRef = db.collection(actualRef);
    }

    async getProductos() {
        const prods = [];

        try {
            const snapshot = await this.productosRef.get();
            snapshot.forEach((doc) => {
                prods.push({ id: doc.id, ...doc.data() });
            });
        } catch (error) {
            console.log("Error Getting Product: ", error);
        }

        return prods;
    }

    async getProducto(id) {
        let producto;

        try {
            const snapshot = await this.productosRef.doc(id).get();
            producto = { id: snapshot.id, ...snapshot.data() };
        } catch (error) {
            console.log("Error Getting Product: ", error);
        }

        return producto;
    }

    async addProducto(producto) {
        // Espera un objeto del tipo: { name: '', price: 0, description: '', size: '', img: '' }
        try {
            await this.productosRef.add(producto);
        } catch (error) {
            console.log("Error Adding Product: ", error);
        }
    }

    async deleteProducto(id) {
        try {
            await this.productosRef.doc(id).delete();
        } catch (error) {
            console.log("Error Deleting Product: ", error);
        }
    }
}

// VARIABLES
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
// Initialize Firestore
const db = firebase.firestore();
$("#awaiting").show();

const getItems = async() => {
    //Consulta firestore
    let prod = new Productos(db, "invitados");
    console.log(prod);
    let pers = "";
    try {
        pers = await prod.getProductos();
    } catch (error) {
        console.log("Error al obtener los productos: " + error);
    } finally {
        console.log("Finalizó la consulta");
        $("#awaiting").hide();
    }

    return pers.sort((a, b) => {
        if (a.nombre > b.nombre) {
            //comparación lexicogŕafica
            return 1;
        } else if (a.nombre < b.nombre) {
            return -1;
        }
        return 0;
    });
};

let invitados = "";
(async() => {
    let option =
        "<option value='' disabled selected>Seleccione un invitadx</option>";
    invitados = await getItems();
    invitados.map((invitado) => {
        if (invitado.confirma !== "true") {
            option =
                option +
                ` <option value="${invitado.nombre}">${invitado.nombre}</option>`;
        }
    });

    console.log("Cargando invitadxs");
    $("#sel1").append(option);
    console.log("Invitadxs cargados");
})();

let confirmar = document.getElementById("confirmar");

confirmar.addEventListener("click", (e) => {
    let nombre = document.getElementById("name").value;
    let menu = document.getElementById("sel2").value;
    let dni = document.getElementById("dni").value;
    let email = document.getElementById("email").value;
    let tel = document.getElementById("tel").value;

    let invitado = invitados.filter((invitado) => {
        return invitado.nombre == nombre;
    });

    if (nombre && menu && dni && email && tel) {
        document.getElementById("buttonDisabled").style.display = "none";
        console.log("Se confirma nombre: " + nombre);
        console.log("ID: " + invitado[0].id);

        console.log(invitado);
        db.collection("invitados").doc(invitado[0].id).update({
            dni: dni,
            confirma: "true",
            menu_especial: menu,
            cel: tel,
            email: email,
        });
        console.log("Acualizando google sheet");
        fetch(
                "https://southamerica-east1-capable-reserve-375504.cloudfunctions.net/belenluis"
            )
            .then((response) => console.log(response.json()))
            .then((data) => console.log(data));
    } else {
        document.getElementById("buttonDisabled").style.display = "block";
    }
});

let togglebutton = () => {
    let nombre = document.getElementById("name").value;
    let menu = document.getElementById("sel2").value;
    let dni = document.getElementById("dni").value;
    let email = document.getElementById("email").value;
    let tel = document.getElementById("tel").value;

    if (nombre && menu && dni && email && tel) {
        document.getElementById("buttonDisabled").style.display = "none";
    } else {
        document.getElementById("buttonDisabled").style.display = "block";
    }
};