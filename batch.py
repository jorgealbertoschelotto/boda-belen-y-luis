import pandas as pd
import firebase_admin
from firebase_admin import credentials, firestore
cred = credentials.Certificate("./key.json")
firebase_admin.initialize_app(cred, 
{
'databaseURL': 'https://boda-bel-luis.firebaseio.com/'
})
db = firestore.client()
doc_ref = db.collection(u'invitados')
# Import data
df = pd.read_excel('INVITADOS.xlsx')
tmp = df.to_dict(orient='records')
list(map(lambda x: doc_ref.add(x), tmp))